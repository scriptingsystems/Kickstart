#!/usr/bin/bash
 
## Define variables
MEM_SIZE=1048       # Memory setting in MiB
VCPUS=2             # CPU Cores count
OS_VARIANT="rhl8.0" # List with osinfo-query  os
#ISO_FILE="/var/lib/libvirt/images/CentOS-Stream-8-x86_64-latest-dvd1.iso" # Path to ISO file
ISO_FILE="/vol_data/isos/rhel-8.4-x86_64-dvd.iso" # Path to ISO file
DISK_SIZE=10 #10 for 10 GB
VIRTUAL_NETWORK="NetworkInternet"

echo -en "Enter vm name: "
read VM_NAME
OS_TYPE="linux"
#echo -en "Enter virtual disk size : "
#read DISK_SIZE
 
sudo virt-install \
     --name ${VM_NAME} \
     --memory=${MEM_SIZE} \
     --vcpus=${VCPUS} \
     --os-type ${OS_TYPE} \
     --location ${ISO_FILE} \
     --disk size=${DISK_SIZE},pool=vol_vm  \
     --network network=${VIRTUAL_NETWORK} \
     --graphics=none \
     --os-variant=${OS_VARIANT} \
     --console pty,target_type=serial \
     --initrd-inject ks.cfg --extra-args "inst.ks=file:/ks.cfg console=tty0 console=ttyS0,115200n8"
     #--extra-args="ks=http://192.168.122.1/ks.cfg console=tty0 console=ttyS0,115200n8"
